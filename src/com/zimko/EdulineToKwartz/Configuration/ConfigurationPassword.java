package com.zimko.EdulineToKwartz.Configuration;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JDesktopPane;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JCheckBox;

import com.zimko.EdulineToKwartz.EdulineToKwartz;

import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Font;

public class ConfigurationPassword extends JDialog {
	private static EdulineToKwartz main;
	private static final long serialVersionUID = 1L;
	private JTextField textFieldExtraChar;
	private int passwordLenght =8;
	private boolean confuseCharacterAllowed =false;
	private boolean numberAllowed =true;
	private boolean uperCaseForBegin =false;
	private boolean lowerCaseForBegin = false;
	private boolean extraCharacterAllowed = false;
	private String extraCharacter ="%\u00E9\u00E8&#\u00E0^\u00E7";
	private JDesktopPane desktopPane;
	private JSpinner spinnerLenghPassword;
	private JCheckBox checkBoxConfuseChar;
	private final JCheckBox checkBoxUperCase;
	private final JCheckBox checkBoxLowerCase;
	private final JCheckBox checkBoxExtraCharAllowed;
	private final JCheckBox checkBoxNumberAllowed;
	/**
	 * Launch the application.
	 */

	public ConfigurationPassword(EdulineToKwartz instance) {
		ConfigurationPassword.main = instance;
		WindowAdapter exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(null, "Etes vous sur de fermer cette fen�tre? Les r�glages ne seront pas sauvegarder", "Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                   main.configurationPassword.setVisible(false);
                }
            }
        };
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(exitListener);
		//set environnement;
		setModal(true);
		setTitle("Configuration du mot de passe");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConfigurationPassword.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						saveOption();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			desktopPane = new JDesktopPane();
			desktopPane.setBackground(Color.LIGHT_GRAY);
			getContentPane().add(desktopPane, BorderLayout.CENTER);
			
			JLabel lblTailleDuMot = new JLabel("Taille du mot de passe:");
			lblTailleDuMot.setFont(new Font("Arial", Font.PLAIN, 11));
			lblTailleDuMot.setBounds(23, 11, 110, 14);
			desktopPane.add(lblTailleDuMot);
			
			spinnerLenghPassword = new JSpinner();
			spinnerLenghPassword.setModel(new SpinnerNumberModel(passwordLenght, 4, 12, 1));
			spinnerLenghPassword.setBounds(139, 8, 39, 20);
			desktopPane.add(spinnerLenghPassword);
			
			checkBoxConfuseChar = new JCheckBox("");
			checkBoxConfuseChar.setSelected(confuseCharacterAllowed);
			checkBoxConfuseChar.setBounds(286, 27, 21, 23);
			desktopPane.add(checkBoxConfuseChar);
			
			JLabel lblDsactiverLesCaractres = new JLabel("Activer les caract\u00E8res confondables (1/I,O 0...)");
			lblDsactiverLesCaractres.setFont(new Font("Arial", Font.PLAIN, 11));
			lblDsactiverLesCaractres.setBounds(23, 36, 262, 14);
			desktopPane.add(lblDsactiverLesCaractres);
			
			JLabel lblLeMotDe = new JLabel("Le mot de passe commence par une lettre majuscule");
			lblLeMotDe.setFont(new Font("Arial", Font.PLAIN, 11));
			lblLeMotDe.setBounds(23, 61, 262, 14);
			desktopPane.add(lblLeMotDe);
			
			checkBoxUperCase = new JCheckBox("");
			checkBoxUperCase.setSelected(uperCaseForBegin);
			checkBoxUperCase.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					//not set at same time upper and lower
					if(checkBoxUperCase.isSelected()){
						checkBoxLowerCase.setSelected(false);
					}
				}
			});
			checkBoxUperCase.setBounds(286, 57, 21, 23);
			desktopPane.add(checkBoxUperCase);
			
			JLabel lblminuscule = new JLabel("/minuscule");
			lblminuscule.setFont(new Font("Arial", Font.PLAIN, 11));
			lblminuscule.setBounds(313, 61, 56, 14);
			desktopPane.add(lblminuscule);
			
			checkBoxLowerCase = new JCheckBox("");
			checkBoxLowerCase.setSelected(lowerCaseForBegin);
			checkBoxLowerCase.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if(checkBoxLowerCase.isSelected()){
						//not set at same time upper and lower
						checkBoxUperCase.setSelected(false);
					}
				}
			});
			checkBoxLowerCase.setBounds(367, 57, 21, 23);
			desktopPane.add(checkBoxLowerCase);
			
			JLabel lblLeMotDe_1 = new JLabel("Le mot de passe peut contenir des chiffres:");
			lblLeMotDe_1.setFont(new Font("Arial", Font.PLAIN, 11));
			lblLeMotDe_1.setBounds(23, 86, 213, 14);
			desktopPane.add(lblLeMotDe_1);
			
			checkBoxNumberAllowed = new JCheckBox("");
			checkBoxNumberAllowed.setSelected(numberAllowed);
			checkBoxNumberAllowed.setBounds(234, 82, 21, 23);
			desktopPane.add(checkBoxNumberAllowed);
			
			JLabel lblLeMotDe_2 = new JLabel("Le mot de passe contient des caract\u00E8res sp\u00E9ciaux:");
			lblLeMotDe_2.setFont(new Font("Arial", Font.PLAIN, 11));
			lblLeMotDe_2.setBounds(23, 111, 251, 14);
			desktopPane.add(lblLeMotDe_2);
			
			checkBoxExtraCharAllowed = new JCheckBox("");
			checkBoxExtraCharAllowed.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					//enable/disable input string extracharacter
					if(checkBoxExtraCharAllowed.isSelected()){
					textFieldExtraChar.setEnabled(true);
					}else{
						textFieldExtraChar.setEnabled(false);
					}
				}
			});
			checkBoxExtraCharAllowed.setBounds(286, 111, 21, 23);
			checkBoxExtraCharAllowed.setSelected(extraCharacterAllowed);
			desktopPane.add(checkBoxExtraCharAllowed);
			
			textFieldExtraChar = new JTextField();
			textFieldExtraChar.setEnabled(false);
			textFieldExtraChar.setText(extraCharacter);
			//check if needed enable true or false
			textFieldExtraChar.setEnabled(extraCharacterAllowed);
			textFieldExtraChar.setBounds(23, 161, 241, 20);
			desktopPane.add(textFieldExtraChar);
			textFieldExtraChar.setColumns(10);
			
			JLabel lblListeDesCaractres = new JLabel("Liste des caract\u00E8res sp\u00E9ciaux:");
			lblListeDesCaractres.setFont(new Font("Arial", Font.PLAIN, 11));
			lblListeDesCaractres.setBounds(23, 136, 262, 14);
			desktopPane.add(lblListeDesCaractres);
		}
	}
	
	public void saveOption(){
		setPasswordLenght((Integer) spinnerLenghPassword.getValue());
		setConfuseCharacterAllowed(checkBoxConfuseChar.isSelected());
		setNumberAllowed(checkBoxNumberAllowed.isSelected());
		setUperCaseForBegin(checkBoxUperCase.isSelected());
		setLowerCaseForBegin (checkBoxLowerCase.isSelected());
		setExtraCharacterAllowed(checkBoxExtraCharAllowed.isSelected());
		setExtraCharacter(textFieldExtraChar.getText());
		//hide
		main.configurationPassword.setVisible(false);
	}
	
	public int getPasswordLenght() {
		return passwordLenght;
	}

	public void setPasswordLenght(int passwordLenght) {
		this.passwordLenght = passwordLenght;
	}

	public boolean getConfuseCharacterAllowed() {
		return confuseCharacterAllowed;
	}

	public void setConfuseCharacterAllowed(boolean confuseCharacterAllowed) {
		this.confuseCharacterAllowed = confuseCharacterAllowed;
	}

	public boolean getNumberAllowed() {
		return numberAllowed;
	}

	public void setNumberAllowed(boolean numberAllowed) {
		this.numberAllowed = numberAllowed;
	}

	public boolean getUperCaseForBegin() {
		return uperCaseForBegin;
	}

	public void setUperCaseForBegin(boolean uperCaseForBegin) {
		this.uperCaseForBegin = uperCaseForBegin;
	}

	public boolean getLowerCaseForBegin() {
		return lowerCaseForBegin;
	}

	public void setLowerCaseForBegin(boolean lowerCaseForBegin) {
		this.lowerCaseForBegin = lowerCaseForBegin;
	}

	public boolean isExtraCharacterAllowed() {
		return extraCharacterAllowed;
	}

	public void setExtraCharacterAllowed(boolean extraCharacterAllowed) {
		this.extraCharacterAllowed = extraCharacterAllowed;
	}

	public String getExtraCharacter() {
		return extraCharacter;
	}

	public void setExtraCharacter(String extraCharacter) {
		this.extraCharacter = extraCharacter;
	}
}
