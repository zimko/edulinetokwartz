package com.zimko.EdulineToKwartz.Configuration;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JDesktopPane;

import java.awt.Color;
import java.awt.Font;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import java.awt.SystemColor;

import javax.swing.JCheckBox;

import com.zimko.EdulineToKwartz.EdulineToKwartz;
import com.zimko.EdulineToKwartz.Configuration.ConfigurationPassword;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFormattedTextField;

public class ConfigurationTypes extends JDialog {

	/**
	 * 
	 */
	private EdulineToKwartz main;
	private static final long serialVersionUID = -4794211421716503839L;
	private java.util.List<String> availablePasswordType = Arrays.asList("Phon�tique","Al�atoire", "jjmmaa", "jjmmaaaa", "jj-mm-aa", "jj-mm-aaaa", "jj/mm/aa", "jj/mm/aaaa");
	private java.util.List<String> availableLoginType = Arrays.asList("nom.pr�nom", "pr�nom.nom");
	private List listChoiceLoginType;
	private List listChoicePasswordType;
	private JCheckBox checkbxAddAleatoryNumber;
	private JLabel lblExempleLogin;
	private JLabel lblExemplePassword;
	private JFormattedTextField txtPrefix;
	private String typePasswordChosed = "Phon�tique";
	private String typeLoginChosed = "nom.pr�nom";
	private boolean addTwoAleatoryNumber = false;
	private String typePasswordChosedTmp = "Phon�tique";
	private String typeLoginChosedTmp = "nom.pr�nom";
	private boolean addTwoAleatoryNumberTmp = false;
	private String defaultPrefix ="cl";

	public ConfigurationTypes(EdulineToKwartz instance) {
		this.main = instance;
		WindowAdapter exitListener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(null, "Etes vous sur de fermer cette fen�tre? Les r�glages ne seront pas sauvegarder", "Exit Confirmation", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					main.configType.setVisible(false);
				}
			}
		};
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(exitListener);
		// set environnement;
		setModal(true);
		setTitle("Configuration des logins et passwords");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ConfigurationPassword.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		setTitle("Configuration \u00E9l\u00E8ves");
		setBounds(100, 100, 615, 510);

		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						saveOption();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			JDesktopPane desktopPane = new JDesktopPane();
			desktopPane.setBackground(Color.LIGHT_GRAY);
			getContentPane().add(desktopPane, BorderLayout.CENTER);
			{
				JLabel lblNewLabel = new JLabel("Le login est de type: ");
				lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 11));
				lblNewLabel.setBounds(10, 36, 100, 14);
				desktopPane.add(lblNewLabel);
			}
			{
				JLabel lblLeMotDe = new JLabel("Le mot de passe est de type:");
				lblLeMotDe.setFont(new Font("Arial", Font.PLAIN, 11));
				lblLeMotDe.setBounds(192, 36, 140, 14);
				desktopPane.add(lblLeMotDe);
			}
			lblExempleLogin = new JLabel("");
			lblExempleLogin.setBounds(446, 137, 131, 14);
			lblExempleLogin.setText(getLoginTmp("GAM�Z", "��lin�-marie"));
			desktopPane.add(lblExempleLogin);

			lblExemplePassword = new JLabel("");
			lblExemplePassword.setBounds(446, 162, 131, 14);
			lblExemplePassword.setText(getPasswordTmp("05021997"));
			desktopPane.add(lblExemplePassword);

			{
				JLabel lblConfigurationDuLogin = new JLabel("Configuration du login pour un \u00E9l\u00E8ve/enseignant ex: \u00C7\u00E9lin\u00E0-marie GAM\u00CAZ n\u00E9e le 14 f\u00E9vrier 1997");
				lblConfigurationDuLogin.setFont(new Font("Arial", Font.BOLD, 11));
				lblConfigurationDuLogin.setBounds(24, 11, 548, 14);
				desktopPane.add(lblConfigurationDuLogin);
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(10, 60, 140, 91);
				desktopPane.add(scrollPane);
				{
					listChoiceLoginType = new List();
					listChoiceLoginType.addItemListener(new ItemListener() {
						public void itemStateChanged(ItemEvent arg0) {
							typeLoginChosedTmp = listChoiceLoginType.getSelectedItem();
							lblExempleLogin.setText(getLoginTmp("GAM�Z", "��lin�-marie"));
						}
					});
					int index = 0;
					for (String choice : availableLoginType) {
						listChoiceLoginType.add(choice);
						// check choice in config and set selected
						if (choice.equals(typeLoginChosed)) {
							listChoiceLoginType.select(index);
						}
						index++;
					}
					listChoiceLoginType.setMultipleMode(false);
					scrollPane.setViewportView(listChoiceLoginType);
				}
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(192, 61, 138, 89);
				desktopPane.add(scrollPane);
				{
					listChoicePasswordType = new List();
					listChoicePasswordType.addItemListener(new ItemListener() {
						public void itemStateChanged(ItemEvent arg0) {
							typePasswordChosedTmp = listChoicePasswordType.getSelectedItem();
							lblExemplePassword.setText(getPasswordTmp("14021997"));
							if (listChoicePasswordType.getSelectedItem().equals("Al�atoire")) {
								checkbxAddAleatoryNumber.setSelected(false);
								addTwoAleatoryNumberTmp = false;
								checkbxAddAleatoryNumber.setEnabled(false);
							} else {
								checkbxAddAleatoryNumber.setEnabled(true);
							}
						}
					});
					int index = 0;
					for (String choice : availablePasswordType) {
						listChoicePasswordType.add(choice);
						// check choice in config and set selected
						if (choice.equals(typePasswordChosed)) {
							listChoicePasswordType.select(index);
						}
						index++;
					}
					scrollPane.setViewportView(listChoicePasswordType);
					listChoicePasswordType.setMultipleMode(false);
				}
			}
			{
				JLabel lblLesCharacteresAccentus = new JLabel("<html><p>Les caract\u00E8res accentu\u00E9s\r\nsont automatiquement supprim\u00E9s ainsi que les espaces et les points</p></html>");
				lblLesCharacteresAccentus.setForeground(Color.RED);
				lblLesCharacteresAccentus.setFont(new Font("Arial", Font.BOLD, 11));
				lblLesCharacteresAccentus.setBounds(363, 87, 197, 39);
				desktopPane.add(lblLesCharacteresAccentus);
			}

			checkbxAddAleatoryNumber = new JCheckBox("Ajout de deux chiffres al\u00E9atoire en fin de login ");
			checkbxAddAleatoryNumber.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent arg0) {
					addTwoAleatoryNumberTmp = checkbxAddAleatoryNumber.isSelected();
					lblExempleLogin.setText(getLoginTmp("GAM�Z", "��lin�-marie"));
				}
			});
			checkbxAddAleatoryNumber.setFont(new Font("Arial", Font.PLAIN, 11));
			checkbxAddAleatoryNumber.setBounds(10, 158, 280, 23);
			checkbxAddAleatoryNumber.setSelected(addTwoAleatoryNumber);
			// disable and set to false if the type off passord already aleatory
			if (typePasswordChosed.equals("Al�atoire")) {
				checkbxAddAleatoryNumber.setSelected(false);
				checkbxAddAleatoryNumber.setEnabled(false);
			} else {
				checkbxAddAleatoryNumber.setEnabled(true);
			}
			desktopPane.add(checkbxAddAleatoryNumber);

			JDesktopPane desktopPane_1 = new JDesktopPane();
			desktopPane_1.setBackground(SystemColor.controlHighlight);
			desktopPane_1.setBounds(10, 218, 579, 210);
			desktopPane.add(desktopPane_1);

			JLabel label = new JLabel("nom: Le nom du compte tout en minuscule");
			label.setHorizontalAlignment(SwingConstants.LEFT);
			label.setVerticalAlignment(SwingConstants.BOTTOM);
			label.setFont(new Font("Arial", Font.ITALIC, 11));
			label.setBounds(10, 10, 210, 14);
			desktopPane_1.add(label);

			JLabel label_1 = new JLabel("al\u00E9atoire: Un mot de passe g\u00E9n\u00E9r\u00E9 al\u00E9atoirement en fonction de la configuration que vous avez choisi");
			label_1.setHorizontalAlignment(SwingConstants.LEFT);
			label_1.setVerticalAlignment(SwingConstants.TOP);
			label_1.setFont(new Font("Arial", Font.ITALIC, 11));
			label_1.setBounds(10, 154, 495, 14);
			desktopPane_1.add(label_1);

			JLabel label_2 = new JLabel("aaaa: L'ann\u00E9e de naissance du propri�taire du compte ");
			label_2.setHorizontalAlignment(SwingConstants.LEFT);
			label_2.setVerticalAlignment(SwingConstants.TOP);
			label_2.setFont(new Font("Arial", Font.ITALIC, 11));
			label_2.setBounds(10, 130, 269, 14);
			desktopPane_1.add(label_2);

			JLabel label_3 = new JLabel("aa: Les deux derniers chiffres de l'ann\u00E9e de naissance du propri�taire du compte ");
			label_3.setHorizontalAlignment(SwingConstants.LEFT);
			label_3.setVerticalAlignment(SwingConstants.TOP);
			label_3.setFont(new Font("Arial", Font.ITALIC, 11));
			label_3.setBounds(10, 106, 395, 14);
			desktopPane_1.add(label_3);

			JLabel label_4 = new JLabel("mm: le jours de naissance du propri�taire du compte");
			label_4.setHorizontalAlignment(SwingConstants.LEFT);
			label_4.setVerticalAlignment(SwingConstants.TOP);
			label_4.setFont(new Font("Arial", Font.ITALIC, 11));
			label_4.setBounds(10, 82, 259, 14);
			desktopPane_1.add(label_4);

			JLabel label_5 = new JLabel("jj: le jours de naissance du propri�taire du compte");
			label_5.setHorizontalAlignment(SwingConstants.LEFT);
			label_5.setVerticalAlignment(SwingConstants.TOP);
			label_5.setFont(new Font("Arial", Font.ITALIC, 11));
			label_5.setBounds(10, 58, 247, 14);
			desktopPane_1.add(label_5);

			JLabel label_7 = new JLabel("pr\u00E9nom: Le pr\u00E9nom du compte tout en minuscule");
			label_7.setHorizontalAlignment(SwingConstants.LEFT);
			label_7.setVerticalAlignment(SwingConstants.TOP);
			label_7.setFont(new Font("Arial", Font.ITALIC, 11));
			label_7.setBounds(10, 34, 242, 14);
			desktopPane_1.add(label_7);
			
			JLabel lblMagicpasswordUnMot = new JLabel("Phon\u00E9tique: Un mot de passe g\u00E9n\u00E9r\u00E9 al\u00E9atoirement de 9 characteres alternant consonnes et voyelles.");
			lblMagicpasswordUnMot.setVerticalAlignment(SwingConstants.TOP);
			lblMagicpasswordUnMot.setHorizontalAlignment(SwingConstants.LEFT);
			lblMagicpasswordUnMot.setFont(new Font("Arial", Font.ITALIC, 11));
			lblMagicpasswordUnMot.setBounds(10, 178, 559, 14);
			desktopPane_1.add(lblMagicpasswordUnMot);
			
			JLabel lbluneMajusculeEst = new JLabel("(Une majuscule est mise au d\u00E9but et un chiffre al\u00E9atoire \u00E0 la fin.)");
			lbluneMajusculeEst.setVerticalAlignment(SwingConstants.TOP);
			lbluneMajusculeEst.setHorizontalAlignment(SwingConstants.LEFT);
			lbluneMajusculeEst.setFont(new Font("Arial", Font.ITALIC, 11));
			lbluneMajusculeEst.setBounds(73, 190, 347, 14);
			desktopPane_1.add(lbluneMajusculeEst);

			JLabel lblLogin = new JLabel("Login : ");
			lblLogin.setFont(new Font("Arial", Font.PLAIN, 11));
			lblLogin.setBounds(363, 137, 46, 14);
			desktopPane.add(lblLogin);

			JLabel lblMotDePasse = new JLabel("Mot de passe:");
			lblMotDePasse.setFont(new Font("Arial", Font.PLAIN, 11));
			lblMotDePasse.setBounds(363, 162, 73, 14);
			desktopPane.add(lblMotDePasse);
			
			JDesktopPane desktopPane_2 = new JDesktopPane();
			desktopPane_2.setBackground(SystemColor.controlHighlight);
			desktopPane_2.setBounds(10, 188, 426, 23);
			desktopPane.add(desktopPane_2);
			
			JLabel label_6 = new JLabel("Prefixe lorsque les classes ne sont compos\u00E9es que de chiffres");
			label_6.setFont(new Font("Arial", Font.PLAIN, 11));
			label_6.setBounds(10, 4, 322, 14);
			desktopPane_2.add(label_6);
			
			txtPrefix = new JFormattedTextField();
			txtPrefix.setBounds(350, 2, 35, 20);
			desktopPane_2.add(txtPrefix);
			txtPrefix.setText(defaultPrefix);			
		}
	}

	private void saveOption() {
		//check prefix max 2 char and not only number
		if(!txtPrefix.getText().matches("[a-zA-z]+")||txtPrefix.getText().length()>2){
			JOptionPane.showMessageDialog(null,"Le pr�fixe doit �tre d'une longueur maximale de 2 et doit �tre compos� de lettre(s) exclusivement","Erreur",JOptionPane.ERROR_MESSAGE);
			setDefaultPrefix("cl");
			txtPrefix.setText(defaultPrefix);
			return;
		}
		setTypePasswordChosed(listChoicePasswordType.getSelectedItem());
		setTypeLoginChosed(listChoiceLoginType.getSelectedItem());
		setAddTwoAleatoryNumber(checkbxAddAleatoryNumber.isSelected());
		setDefaultPrefix(txtPrefix.getText());
		main.configType.setVisible(false);
	}

	public String getLogin(String name, String forname) {
		//random not egal at 99 because two number is absolutely needed 
		String randomNumber = String.valueOf(new Random().nextInt(9)) + String.valueOf(new Random().nextInt(9));
		if (!addTwoAleatoryNumber) {
			randomNumber = "";
		}
		name = Normalizer.normalize(name.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("[\\W_]", "");
		forname = Normalizer.normalize(forname.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("[\\W_]", "");
		switch (typeLoginChosed) {
		case "nom.pr�nom":
			return name + "." + forname + randomNumber;
			// inputval.substring(0,1).toUpperCase() +
			// inputval.substring(1).toLowerCase();
		case "pr�nom.nom":
			return forname + "." + name + randomNumber;
		default:
			return name + "." + forname + randomNumber;
		}
	}

	public String getPassword(String yearOfBirth) {
		if (yearOfBirth.length() != 8) {
			yearOfBirth = "00000000";
		}
		//add first zero if needed!!
		String day = yearOfBirth.substring(0, 2).replaceAll("^[0-9]{1}$", "\n0$&");
		String month = yearOfBirth.substring(2, 4);
		String fullYear = yearOfBirth.substring(4, 8);
		String littleYear = yearOfBirth.substring(6, 8);
		switch (typePasswordChosed) {
		case "Phon�tique":
			return main.randomPassword.getMagicPassword();
		case "Al�atoire":
			return main.randomPassword.getRandomPassword();
		case "jjmmaa":
			return day + month + littleYear;
		case "jjmmaaaa":
			return day + month + fullYear;
		case "jj-mm-aa":
			return day + "-" + month + "-" + littleYear;
		case "jj-mm-aaaa":
			return day + "-" + month + "-" + fullYear;
		case "jj/mm/aa":
			return day + "/" + month + "/" + littleYear;
		case "jj/mm/aaaa":
			return day + "/" + month + "/" + fullYear;
		default:
			return main.randomPassword.getRandomPassword();
		}
	}

	public String getLoginTmp(String name, String forname) {
		String randomNumber = String.valueOf(new Random().nextInt(99));
		if (!addTwoAleatoryNumberTmp) {
			randomNumber = "";
		}
		name = Normalizer.normalize(name.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("[\\W_]", "");
		forname = Normalizer.normalize(forname.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("[\\W_]", "");
		switch (typeLoginChosedTmp) {
		case "nom.pr�nom":
			return name + "." + forname + randomNumber;
			// inputval.substring(0,1).toUpperCase() +
			// inputval.substring(1).toLowerCase();
		case "pr�nom.nom":
			return forname + "." + name + randomNumber;
		default:
			return name + "." + forname + randomNumber;
		}
	}

	public String getPasswordTmp(String yearOfBirth) {
		String day = yearOfBirth.substring(0, 2).replaceAll("^[0-9]{1}$", "\n0$&");
		String month = yearOfBirth.substring(2, 4);
		String fullYear = yearOfBirth.substring(4, 8);
		String littleYear = yearOfBirth.substring(6, 8);
		switch (typePasswordChosedTmp) {
		case "Phon�tique":
			return main.randomPassword.getMagicPassword();
		case "Al�atoire":
			return main.randomPassword.getRandomPassword();
		case "jjmmaa":
			return day + month + littleYear;
		case "jjmmaaaa":
			return day + month + fullYear;
		case "jj-mm-aa":
			return day + "-" + month + "-" + littleYear;
		case "jj-mm-aaaa":
			return day + "-" + month + "-" + fullYear;
		case "jj/mm/aa":
			return day + "/" + month + "/" + littleYear;
		case "jj/mm/aaaa":
			return day + "/" + month + "/" + fullYear;
		default:
			return main.randomPassword.getRandomPassword();
		}
	}

	public String getTypePasswordChosed() {
		return typePasswordChosed;
	}

	public void setTypePasswordChosed(String typePasswordChosed) {
		this.typePasswordChosed = typePasswordChosed;
	}

	public String getTypeLoginChosed() {
		return typeLoginChosed;
	}

	public void setTypeLoginChosed(String typeLoginChosed) {
		this.typeLoginChosed = typeLoginChosed;
	}

	public boolean isAddTwoAleatoryNumber() {
		return addTwoAleatoryNumber;
	}

	public void setAddTwoAleatoryNumber(boolean addTwoAleatoryNumber) {
		this.addTwoAleatoryNumber = addTwoAleatoryNumber;
	}

	public void getChoiceTypeLogin(String choice) {

	}

	public String getDefaultPrefix() {
		return defaultPrefix;
	}

	public void setDefaultPrefix(String defaultPrefix) {
		this.defaultPrefix = defaultPrefix;
	}
}
