package com.zimko.EdulineToKwartz.Configuration;

import java.util.Random;

import com.zimko.EdulineToKwartz.EdulineToKwartz;

public class RandomPassword {
	private ConfigurationPassword configurationPassword;

	public RandomPassword(EdulineToKwartz instance) {
		this.configurationPassword = instance.configurationPassword;
	}

	public String getRandomPassword() {
		int passwordLenght = configurationPassword.getPasswordLenght();
		boolean confuseCharacterAllowed = configurationPassword.getConfuseCharacterAllowed();
		boolean numberAllowed = configurationPassword.getNumberAllowed();
		boolean uperCaseForBegin = configurationPassword.getUperCaseForBegin();
		boolean lowerCaseForBegin = configurationPassword.getLowerCaseForBegin();
		boolean extraCharacterAllowed = configurationPassword.isExtraCharacterAllowed();
		String extraCharacter = configurationPassword.getExtraCharacter();
		
		String fullNumber = "0123456789";
		String limitedNumber ="23456789";
		String fullUperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String limitedUperCase ="ABCDEFGHJKMNPQRSTUVWXYZ";
		String fullLowerCase = "abcdefghijklmnopqrstuvwxyz";
		String limitedLowerCase = "abcdefghjkmnpqrstuvwxyz";
		String randomPassword = null;
		String generatedString;
		if(confuseCharacterAllowed){
			generatedString = fullUperCase+fullLowerCase;
		}else{
			generatedString = limitedLowerCase+limitedUperCase;
		}
		if(numberAllowed){
			if(confuseCharacterAllowed){
				generatedString = generatedString+fullNumber;
			}else{
				generatedString = generatedString+limitedNumber;
			}
		}
		if(extraCharacterAllowed){
			generatedString = generatedString+extraCharacter;
		}
		for(int i=1;i<=passwordLenght;i++){
			//get first character
			if(i==1){
				randomPassword = getRandomCharacter(generatedString);
				//check specific first character;
				if(uperCaseForBegin){
					if(confuseCharacterAllowed){
						randomPassword = getRandomCharacter(fullUperCase);
					}else{
						randomPassword = getRandomCharacter(limitedUperCase);
					}
				}
				if(lowerCaseForBegin){
					if(confuseCharacterAllowed){
						randomPassword = getRandomCharacter(fullLowerCase);
					}else{
						randomPassword = getRandomCharacter(limitedLowerCase);
					}
				}
			}else{
				//next Character
				randomPassword = randomPassword+getRandomCharacter(generatedString);
			}
		}
		return randomPassword;
	}
	
	public String getMagicPassword(){
		String magicPassword = "";
		String limitedNumber ="23456789";
		String uperConsonants ="BCDFGHJKMNPQRSTVWXZ";
		String lowerConsonants ="bcdfghjkmnpqrstvwxz";
		String lowervowels="aeuy";
		magicPassword+=getRandomCharacter(uperConsonants);
		magicPassword+=getRandomCharacter(lowervowels);	
		magicPassword+=getRandomCharacter(lowerConsonants);	
		magicPassword+=getRandomCharacter(lowervowels);	
		magicPassword+=getRandomCharacter(lowerConsonants);	
		magicPassword+=getRandomCharacter(lowervowels);	
		magicPassword+=getRandomCharacter(lowerConsonants);	
		magicPassword+=getRandomCharacter(lowervowels);		
		magicPassword+=getRandomCharacter(limitedNumber);	
		return magicPassword;
	}
	
	private String getRandomCharacter(String generatedString){
		String randomCharacter=null;
		int rnd = new Random().nextInt(generatedString.length());
		char rndChar = generatedString.charAt(rnd);
		randomCharacter = Character.toString(rndChar);
		return randomCharacter;
	}
}
