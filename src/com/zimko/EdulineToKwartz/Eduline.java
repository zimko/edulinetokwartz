package com.zimko.EdulineToKwartz;

public class Eduline {
	private String name="";
	private String realName ="";
	private String forname ="";
	private String realForname ="";
	private String division ="";
	private String realDivision ="";
	private String birday ="";
	private String realBirday ="";
	private Boolean isNew = true;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForname() {
		return forname;
	}
	public void setForname(String forname) {
		this.forname = forname;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getBirday() {
		return birday;
	}
	public void setBirday(String birday) {
		this.birday = birday;
	}
	public Boolean isNew() {
		return isNew;
	}
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getRealForname() {
		return realForname;
	}
	public void setRealForname(String realForname) {
		this.realForname = realForname;
	}
	public String getRealDivision() {
		return realDivision;
	}
	public void setRealDivision(String realDivision) {
		this.realDivision = realDivision;
	}
	public String getRealBirday() {
		return realBirday;
	}
	public void setRealBirday(String realBirday) {
		this.realBirday = realBirday;
	}
}
