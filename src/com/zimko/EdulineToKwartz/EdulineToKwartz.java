package com.zimko.EdulineToKwartz;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.zimko.EdulineToKwartz.Configuration.ConfigurationPassword;
import com.zimko.EdulineToKwartz.Configuration.RandomPassword;
import com.zimko.EdulineToKwartz.Update.AvailableUpdate;
import com.zimko.EdulineToKwartz.Update.OpenBrowser;
import com.zimko.EdulineToKwartz.Configuration.ConfigurationTypes;
import com.zimko.EdulineToKwartz.FilesManagement.FileIO;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JDesktopPane;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import net.java.balloontip.BalloonTip;
import net.java.balloontip.styles.BalloonTipStyle;
import net.java.balloontip.styles.EdgedBalloonStyle;
import net.java.balloontip.utils.TimingUtils;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JToggleButton;

public class EdulineToKwartz {
	public final double version = 1.92;
	public final String versioning="<html>"+
	"V1.92 <br>"+
	"* Impl�mentation des mots de passe phonetiques.<br>"+
	"V1.91 <br>"+
	"* Probl�me d'encodage.<br>"+
	"V1.9 <br>"+
	"* Correction bug majeur: le choix nom.prenom n'etais pas prix en compte.<br>"+
	"V1.8 <br>"+
	"* Changement du systeme d'affichage du change-log<br>"+
	"* Changement hebergeur<br>"+
	"V1.7 <br>"+
	"* Ajout d'un fichier pour le publipostage<br>"+
	"* Correction lorsque l'utilisateur ne sauvegarde pas son fichier<br>"+
	"V1.6 <br>"+
	"* Ajout d'un prefix au nom de la division quand celui ci ne contient que des chiffres (Merci Kwartz)<br>"+
	"* Ajout de la configuration du pr�fix<br>"+
	"* Bug corrig� lorsque le fichier d'import de �duline ne contient pas la division<br>"+
	"V1.5 <br>"+
	"* Ajout de l'option pour remettre les m�mes nom et pr�nom dans Kwartz que le fichier d'Eduline<br>"+
	"* Prise en charge de tous caract�res sp�ciaux dans les noms et pr�nom<br>"+
	"* Am�lioration du controle des comptes existant et ceux � rajouter <br>"+
	"* Normalisation des noms en majuscule et pr�nom avec chaque debut en majuscule et le restant en minuscule<br>"+
	"V1.4 <br>"+
	"* Prise en charge des r�seaux sous proxy pour la v�rification de version<br>"+
	"* Rajout d'un 0 en d�but de mot de passe quand le jours de naissance ne comprenais qu'un seul chiffre<br>"+
	"V1.3 <br>"+
	"* Prise en charge des caract�res ' dans les noms et pr�nom<br>"+
	"* Ajout de la bulle information des r�visions du logiciel<br>"+
	"V1.2 <br>"+
	"* Correction bug mineur<br>"+
	"* Correction orthographique<br>"+
	"V1.1 <br>"+
	"* Version initiale<br></html>";
	
	private JFrame frmConversionDeEduline;
	private boolean jframeLoaded =false;
	public ConfigurationPassword configurationPassword = new ConfigurationPassword(this);
	public RandomPassword randomPassword = new RandomPassword(this);
	public ConfigurationTypes configType = new ConfigurationTypes(this);
	private FileIO fileIO = new FileIO();
	private JTextField textFieldFileImportEleve;
	private JTextField textFieldFileImportKwartz;
	private String fileImportEduline = null;
	private String fileImportKwartz = null;
	private String fileImportPassword = null;
	private String pathForExportFile = null;
	private boolean helpActive = false;
	private boolean externalPasswordFileEnable = false;
	private boolean fileImportEdulineChoose = false;
	private boolean fileImportKwartzChoose = false;
	private boolean fileImportPasswordChoose = false;
	private JButton btnStartConversion;
	private JScrollPane scrollPaneSelectGoodClasse;
	private JList<String> listSelectGoodClasse;
	private JLabel lblSelectGoodClasse;
	private JCheckBox chckbxResetAllPassword;
	private JCheckBox chckbxResetAllLogin;
	private JCheckBox chckbxResetKwarzNameAndForname;
	private List<String[]> csvReaderKwartzList;
	private List<String[]> csvReaderEdulineList;
	private List<String[]> csvReaderPasswordList;
	// set default to -1 (never exist)
	private int idCollunGroupKwartz = -1;
	private int idCollunNameKwartz = -1;
	private int idCollunForNameKwartz = -1;
	private int idCollunLoginKwartz = -1;
	private int idCollunGroupEduline = -1;
	private int idCollunNameEduline = -1;
	private int idCollunForNameEduline = -1;
	private int idCollunBirdayEduline = -1;
	private int idCollunLoginPassword = -1;
	private int idCollunPasswordPassword = -1;
	private BalloonTip balloonTipImportEduline;
	private BalloonTip balloonTipImportKwartz;
	private BalloonTip balloonTipImportPassword;
	private BalloonTip ballonTipSetImportPassword;
	private BalloonTip ballonTipSetNewPassword;
	private BalloonTip ballonTipSetNewLogin;
	private BalloonTip balloonInformationVersioning;
	private BalloonTip balloonTipResetNameAndForname;
	HashMap<String, Kwartz> hashImportByKwartzLogin = new HashMap<>();
	HashMap<String, Eduline> hashImportByEdulineLogin = new HashMap<>();
	HashMap<String, String> hashImportByPassordLogin = new HashMap<>();
	ArrayList<Publiposting> listExportPubliposting = new ArrayList<>();
	private String toolTipHelpImportEduline = "<html> Votre fichier d'export r�alis� via le portail acad�mique. <br >" + "Il doit contenir la liste de vos �l�ves."
			+ " <br> Les colones doivent �tre <i>NOM</i>,<i>PRENOM</i>,<i>DIV.</i>,<i>NE(E) LE</i><br>" + "ou  <i>Nom</i>,<i>Pr�nom 1</i>,<i>Division</i>,<i>Date de naissance</i> .<br>"
			+ "Le nom du fichier n'est pas important.</html>";
	private String toolTipHelpImportKwartz = "<html> Votre fichier d'export kwartz r�alis� via kwartz  <br >" + "Pour l'obtenir allez dans le panel <i>Kwartz</i> rubrique <i>Utilisateurs</i><br>"
			+ "cliquez sur <i>Exportez</i> et suivez les instructions.<br>" + "Vous r�cupererez votre fichier dans le <i>h:\\travail</i> de votre compte winadmin<br>"
			+ "dans le dossier <i>Liste utilisateur</i>.<br> " + "Il sera sous la forme: <i> datedujour.export.txt</i>.</html>";
	private String toolTipHelpImportPassword = "<html> Votre fichier de mot de passe r�alis� l'ann�e pr�c�dente.<br >" + " <br> Les colones doivent �tre <i>login</i>,<i>password</i><br>";
	private String toolTipHelpSetImportPassword = "<html>Option si vous possedez un ancien fichier<br>avec les mots de passe de vos utilisateurs<br>r�alis� l'ann�e pr�c�dente</html>";
	private String toolTipHelpSetnewPassword = "<html>Option si vous souhaitez reset tous les mots de passe de vos utilisateurs</html>";
	private String toolTipHelpSetnewLogin = "<html>Option si vous souhaitez reset tous les logins de vos utilisateurs</html>";
	private String toolTipHelpResetNameAndForname ="<html>Option pour remettre le m�me nom et pr�nom que votre fichier Eduline dans kwartz <br>"
			+"La mise en forme des noms et pr�noms est conserv�e<br>"
			+ "<i>(Ne s'applique qu'aux comptes modifi�s)</i></html>";
	private String log = "";
	private String endLog = "\r\nPour utiliser ses deux fichiers g�n�r�s allez dans Kwartz\r\n" + "rubrique Utiliseurs/Gestion des comptes puis cliquez sur 'importer'\r\n"
			+ "puis importez vos deux fichiers grace � l'option 'transferer le fichier.\r\n'"
			+ "Enfin s�lectionner votre fichier 'Liste_des_�l�ves_�_ajouter_le_xx-xx-xx' et choisissez comme option 'Ajouter les utilisateur'\r\n " + "cliquez sur 'Importer le fichier.\r\n"
			+ "Faite vos r�glage comme cela vous est n�cessaire.\r\n" + "Si vous aviez un ancien fichier de mot de passe et/ou si vous avez choisi de r�initialisez vos mot de passe\r\n"
			+ "mettre l'option 'Mot de passe :' sur 'le changer'."
			+ "Faite la m�me chose avec votre fichier 'Liste_des_�l�ves_�_modifier_le_xx-xx-xx' mais cette fois utilisez l'option 'Mettre � jours les utilisateurs.\r\n\r\n"
			+ "Un fichier pour le publipostage est � votre disposition.A vous de le trier manuellement.\r\n\r\n"
			+ "Les anciens comptes ne sont pas supprim�s mais simplement d�plac�s dans 'compte_a_verif' de kwartz, � vous de les supprimer apr�s controle.\r\n\r\n"
			+ "N'hesitez pas � me faire toutes remarques/demandes par email.\r\n";
	private JLabel lblVersion;
	public JLabel lblUpdateAvailable;
	public JLabel lblLinkUpdate;
	private JButton btnChoseImportOldPassword;
	private JCheckBox chckbxEnableExternalPassword;
	private JTextField textFieldFileImportPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// set UIManager to ave windows look (for open,save and warning message
		// and may be more !!)
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (InstantiationException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {

			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {

			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EdulineToKwartz window = new EdulineToKwartz();
					window.frmConversionDeEduline.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EdulineToKwartz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConversionDeEduline = new JFrame();
		frmConversionDeEduline.setTitle("Conversion de Eduline vers Kwartz");
		frmConversionDeEduline.setIconImage(new ImageIcon(getClass().getResource("/EdulineToKwartz.png")).getImage());
		frmConversionDeEduline.setBounds(100, 100, 764, 427);
		frmConversionDeEduline.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JMenuBar menuBar = new JMenuBar();
		frmConversionDeEduline.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Configuration");
		menuBar.add(mnNewMenu);

		JMenuItem menuItemConfigPassword = new JMenuItem("Configuration du mot de passe al\u00E9atoire");
		menuItemConfigPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				configurationPassword.setVisible(true);
			}
		});
		mnNewMenu.add(menuItemConfigPassword);

		JMenuItem menuItemConfigurationType = new JMenuItem("Configuration des logins et mots de passe");
		menuItemConfigurationType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				configType.setVisible(true);
			}
		});
		mnNewMenu.add(menuItemConfigurationType);

		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setToolTipText("");
		desktopPane.setBackground(Color.GRAY);
		frmConversionDeEduline.getContentPane().add(desktopPane, BorderLayout.CENTER);

		final JButton btnChoseFileEleve = new JButton("Choix du fichier \u00E9l\u00E8ves");
		btnChoseFileEleve.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				if (helpActive) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					balloonTipImportEduline = new BalloonTip(btnChoseFileEleve, toolTipHelpImportEduline, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				if (helpActive) {
					TimingUtils.showTimedBalloon(balloonTipImportEduline, 500);
				}
			}
		});
		btnChoseFileEleve.setFont(new Font("Arial", Font.PLAIN, 11));
		btnChoseFileEleve.setBounds(20, 11, 151, 30);
		desktopPane.add(btnChoseFileEleve);
		btnChoseFileEleve.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseImportEdulineAction();
			}
		});

		btnStartConversion = new JButton("Conversion");
		btnStartConversion.setEnabled(false);
		btnStartConversion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				demarage();
			}
		});
		btnStartConversion.setFont(new Font("Arial", Font.PLAIN, 11));
		btnStartConversion.setBounds(20, 160, 89, 23);
		desktopPane.add(btnStartConversion);

		chckbxResetAllPassword = new JCheckBox("R\u00E9initialisation de tous les mots de passe des \u00E9l\u00E8ves.");
		chckbxResetAllPassword.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if (helpActive && chckbxResetAllPassword.isEnabled()) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					ballonTipSetNewPassword = new BalloonTip(chckbxResetAllPassword, toolTipHelpSetnewPassword, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive && chckbxResetAllPassword.isEnabled()) {
					TimingUtils.showTimedBalloon(ballonTipSetNewPassword, 300);
				}
			}
		});
		chckbxResetAllPassword.setFont(new Font("Arial", Font.PLAIN, 11));
		chckbxResetAllPassword.setBounds(20, 190, 297, 23);
		chckbxResetAllPassword.setEnabled(false);
		desktopPane.add(chckbxResetAllPassword);

		chckbxResetAllLogin = new JCheckBox("R\u00E9initialisation de tous les logins des \u00E9l\u00E8ves.");
		chckbxResetAllLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if (helpActive && chckbxResetAllLogin.isEnabled()) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					ballonTipSetNewLogin = new BalloonTip(chckbxResetAllLogin, toolTipHelpSetnewLogin, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive && chckbxResetAllLogin.isEnabled()) {
					TimingUtils.showTimedBalloon(ballonTipSetNewLogin, 300);
				}
			}
		});
		chckbxResetAllLogin.setFont(new Font("Arial", Font.PLAIN, 11));
		chckbxResetAllLogin.setEnabled(false);
		chckbxResetAllLogin.setBounds(20, 216, 297, 23);
		desktopPane.add(chckbxResetAllLogin);

		final JButton btnChoseImportKwartz = new JButton("Choix du fichier import de kwartz\r\n");
		btnChoseImportKwartz.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				if (helpActive) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					balloonTipImportKwartz = new BalloonTip(btnChoseImportKwartz, toolTipHelpImportKwartz, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive) {
					TimingUtils.showTimedBalloon(balloonTipImportKwartz, 300);
				}
			}
		});
		btnChoseImportKwartz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				chooseImportKwartzAction();
			}
		});

		btnChoseImportKwartz.setFont(new Font("Arial", Font.PLAIN, 11));
		btnChoseImportKwartz.setBounds(20, 52, 201, 30);
		desktopPane.add(btnChoseImportKwartz);

		textFieldFileImportEleve = new JTextField();
		textFieldFileImportEleve.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				checkIsAllFileSelected();
			}
		});
		textFieldFileImportEleve.setText("Non choisi");
		textFieldFileImportEleve.setForeground(Color.RED);
		textFieldFileImportEleve.setFont(new Font("Arial", Font.BOLD, 11));
		textFieldFileImportEleve.setEditable(false);
		textFieldFileImportEleve.setBounds(181, 16, 320, 20);
		desktopPane.add(textFieldFileImportEleve);
		textFieldFileImportEleve.setColumns(10);

		textFieldFileImportKwartz = new JTextField();
		textFieldFileImportKwartz.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				checkIsAllFileSelected();
			}
		});
		textFieldFileImportKwartz.setText("Non choisi");
		textFieldFileImportKwartz.setForeground(Color.RED);
		textFieldFileImportKwartz.setFont(new Font("Arial", Font.BOLD, 11));
		textFieldFileImportKwartz.setEditable(false);
		textFieldFileImportKwartz.setColumns(10);
		textFieldFileImportKwartz.setBounds(231, 57, 320, 20);
		desktopPane.add(textFieldFileImportKwartz);

		scrollPaneSelectGoodClasse = new JScrollPane();
		scrollPaneSelectGoodClasse.setBounds(582, 68, 156, 287);
		scrollPaneSelectGoodClasse.setVisible(false);
		desktopPane.add(scrollPaneSelectGoodClasse);

		listSelectGoodClasse = new JList<String>();
		listSelectGoodClasse.setListData(new String[] { "c6test", "c6test1" });
		listSelectGoodClasse.setFont(new Font("Arial", Font.PLAIN, 11));
		scrollPaneSelectGoodClasse.setViewportView(listSelectGoodClasse);

		lblSelectGoodClasse = new JLabel("<html>S\u00E9lections des classes contenant\r\n</br>les \u00E9l\u00E8ves \u00E0 modifier.</html>");
		lblSelectGoodClasse.setForeground(Color.GREEN);
		lblSelectGoodClasse.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectGoodClasse.setFont(new Font("Arial", Font.BOLD, 11));
		lblSelectGoodClasse.setBounds(582, 0, 153, 57);
		lblSelectGoodClasse.setVisible(false);
		desktopPane.add(lblSelectGoodClasse);

		JLabel lblNewLabel = new JLabel(
				"<html><font color=black>Logiciel cr\u00E9e par DELBECQ Pascal TICE au coll\u00E8ge Marie Curie de Saint Amant Les Eaux et au coll\u00E8ge Germinal de Raismes.<br>\r\nMail :</font> <font color=blue>Pascal.Delbecq@ac-lille.fr</font>\r\n</html>");
		lblNewLabel.setBackground(Color.LIGHT_GRAY);
		lblNewLabel.setBounds(20, 246, 414, 50);
		desktopPane.add(lblNewLabel);

		lblVersion = new JLabel("Version: " + version);
		lblVersion.setBounds(20, 297, 68, 14);
		desktopPane.add(lblVersion);

		lblUpdateAvailable = new JLabel("Aucune nouvelle version disponible");
		lblUpdateAvailable.setBackground(Color.GRAY);
		lblUpdateAvailable.setForeground(Color.CYAN);
		lblUpdateAvailable.setFont(new Font("Arial", Font.BOLD, 12));
		lblUpdateAvailable.setBounds(20, 312, 481, 20);
		desktopPane.add(lblUpdateAvailable);

		lblLinkUpdate = new JLabel("Aucune nouvelle version disponible");
		lblLinkUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new OpenBrowser(lblLinkUpdate.getText());
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lblLinkUpdate.setForeground(Color.BLUE);
				lblLinkUpdate.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblLinkUpdate.setForeground(new Color(127, 255, 0));
				lblLinkUpdate.setCursor(Cursor.getDefaultCursor());
			}
		});
		lblLinkUpdate.setForeground(new Color(127, 255, 0));
		lblLinkUpdate.setFont(new Font("Arial", Font.BOLD, 12));
		lblLinkUpdate.setBackground(Color.GRAY);
		lblLinkUpdate.setBounds(20, 335, 481, 20);
		lblLinkUpdate.setVisible(false);
		desktopPane.add(lblLinkUpdate);

		btnChoseImportOldPassword = new JButton("Choix du fichier externe de mots de passe");
		btnChoseImportOldPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				choosePasswordAction();
			}
		});
		btnChoseImportOldPassword.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				if (helpActive && externalPasswordFileEnable) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					balloonTipImportPassword = new BalloonTip(btnChoseImportOldPassword, toolTipHelpImportPassword, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive && externalPasswordFileEnable) {
					TimingUtils.showTimedBalloon(balloonTipImportPassword, 300);
				}
			}
		});
		btnChoseImportOldPassword.setEnabled(false);
		btnChoseImportOldPassword.setFont(new Font("Arial", Font.PLAIN, 11));
		btnChoseImportOldPassword.setBounds(20, 119, 242, 30);
		desktopPane.add(btnChoseImportOldPassword);

		chckbxEnableExternalPassword = new JCheckBox("Je poss\u00E8de un fichier ind\u00E9pendant avec les mots de passe actuel des \u00E9l\u00E8ves kwartz");
		chckbxEnableExternalPassword.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if (helpActive) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					ballonTipSetImportPassword = new BalloonTip(chckbxEnableExternalPassword, toolTipHelpSetImportPassword, edgedLook, false);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive) {
					TimingUtils.showTimedBalloon(ballonTipSetImportPassword, 300);
				}
			}
		});
		chckbxEnableExternalPassword.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (chckbxEnableExternalPassword.isSelected()) {
					textFieldFileImportPassword.setEnabled(true);
					btnChoseImportOldPassword.setEnabled(true);
					externalPasswordFileEnable = true;
					checkIsAllFileSelected();
				} else {
					textFieldFileImportPassword.setEnabled(false);
					btnChoseImportOldPassword.setEnabled(false);
					externalPasswordFileEnable = false;
					checkIsAllFileSelected();
				}
			}
		});
		chckbxEnableExternalPassword.setSelected(false);
		chckbxEnableExternalPassword.setFont(new Font("Arial", Font.PLAIN, 11));
		chckbxEnableExternalPassword.setBounds(20, 89, 462, 23);
		desktopPane.add(chckbxEnableExternalPassword);

		textFieldFileImportPassword = new JTextField();
		textFieldFileImportPassword.setEnabled(false);
		textFieldFileImportPassword.setText("Non choisi");
		textFieldFileImportPassword.setForeground(Color.RED);
		textFieldFileImportPassword.setFont(new Font("Arial", Font.BOLD, 11));
		textFieldFileImportPassword.setEditable(false);
		textFieldFileImportPassword.setColumns(10);
		textFieldFileImportPassword.setBounds(265, 124, 307, 20);
		desktopPane.add(textFieldFileImportPassword);
		
		chckbxResetKwarzNameAndForname= new JCheckBox("R\u00E9initialisation des nom et pr\u00E9nom de kwartz.");
		chckbxResetKwarzNameAndForname.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				if (helpActive && chckbxResetKwarzNameAndForname.isSelected()) {
					BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
					balloonTipResetNameAndForname = new BalloonTip(chckbxResetKwarzNameAndForname, toolTipHelpResetNameAndForname, edgedLook, false);
				}
			}
			@Override
			public void mouseExited(MouseEvent e) {
				if (helpActive && chckbxResetKwarzNameAndForname.isSelected()) {
					TimingUtils.showTimedBalloon(balloonTipResetNameAndForname, 300);
				}
			}
		});
		chckbxResetKwarzNameAndForname.setFont(new Font("Arial", Font.PLAIN, 11));
		chckbxResetKwarzNameAndForname.setEnabled(false);
		chckbxResetKwarzNameAndForname.setBounds(326, 190, 250, 23);
		desktopPane.add(chckbxResetKwarzNameAndForname);

		final JToggleButton tglbtnNewToggleButton = new JToggleButton("Activer l'aide");
		tglbtnNewToggleButton.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (tglbtnNewToggleButton.isSelected()) {
					helpActive = true;
				} else {
					helpActive = false;
				}
			}
		});
		tglbtnNewToggleButton.setFont(new Font("Arial", Font.PLAIN, 11));
		tglbtnNewToggleButton.setBounds(430, 216, 107, 23);
		desktopPane.add(tglbtnNewToggleButton);
		
		ImageIcon iconInfo = new ImageIcon(getClass().getResource("/information.png"));
		final JLabel LbInformationVersioning = new JLabel(iconInfo);
		LbInformationVersioning.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				BalloonTipStyle edgedLook = new EdgedBalloonStyle(Color.WHITE, Color.BLUE);
				balloonInformationVersioning = new BalloonTip(LbInformationVersioning, "Cliquez ici pour voir le change log.", edgedLook, false);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				TimingUtils.showTimedBalloon(balloonInformationVersioning, 1);
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new VersionGui(versioning);
			}
		});
		LbInformationVersioning.setBounds(82, 293, 39, 23);
		desktopPane.add(LbInformationVersioning);
		// start update
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		executorService.execute(new AvailableUpdate(this));
		jframeLoaded = true;
	}

	private void checkIsAllFileSelected() {
		//check if jframe loaded
		if(!jframeLoaded){
			return;
		}
		// set false by default
		btnStartConversion.setEnabled(false);
		chckbxResetAllPassword.setEnabled(false);
		chckbxResetAllLogin.setEnabled(false);
		chckbxResetKwarzNameAndForname.setEnabled(false);
		// check if file are selected to enable buton and option
		if (externalPasswordFileEnable && !fileImportPasswordChoose) {
			return;
		}
		if (!fileImportEdulineChoose) {
			return;
		}
		if (!fileImportKwartzChoose) {
			return;
		}
		btnStartConversion.setEnabled(true);
		chckbxResetAllPassword.setEnabled(true);
		chckbxResetAllLogin.setEnabled(true);
		chckbxResetKwarzNameAndForname.setEnabled(true);

	}

	private void chooseImportKwartzAction() {
		// set all of by default
		hashImportByKwartzLogin = new HashMap<>();
		csvReaderKwartzList = new ArrayList<>();
		fileImportKwartzChoose = false;
		textFieldFileImportKwartz.setText("Non choisi");
		textFieldFileImportKwartz.setForeground(Color.RED);
		scrollPaneSelectGoodClasse.setVisible(false);
		lblSelectGoodClasse.setVisible(false);
		// update checkallfile to set default
		checkIsAllFileSelected();
		// creater filters and get fileImport
		String[] filters = { "txt", "csv" };
		fileImportKwartz = fileIO.getOpenFilePath("Fichier Kwartz", filters);
		if (fileImportKwartz == null) {
			// not choose send return message
			JOptionPane.showMessageDialog(null, "Merci de choisir un fichier", "Invalide", JOptionPane.WARNING_MESSAGE);
			return;
		}
		// create reader and iterate
		try {
			CSVReader csvReaderKwartz = new CSVReader(new FileReader(fileImportKwartz), ';');
			String[] firstLine;
			// put file to a list<String[]>
			csvReaderKwartzList = csvReaderKwartz.readAll();
			csvReaderKwartz.close();
			// read header and check if all collun present
			// check first if header exist !!
			if (csvReaderKwartzList.size() == 0) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import kwartz n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			firstLine = csvReaderKwartzList.get(0);
			// iterate header
			// first reset to -1 id !!!
			idCollunGroupKwartz = -1;
			idCollunLoginKwartz = -1;
			idCollunNameKwartz = -1;
			idCollunForNameKwartz = -1;
			for (int i = 0; i < firstLine.length; i++) {
				if (firstLine[i].equalsIgnoreCase("groupe d'affectation")) {
					idCollunGroupKwartz = i;
				}
				if (firstLine[i].equalsIgnoreCase("login actuel")) {
					idCollunLoginKwartz = i;
				}
				if (firstLine[i].equalsIgnoreCase("#Nom")) {
					idCollunNameKwartz = i;
				}
				if (firstLine[i].equalsIgnoreCase("Pr�nom")) {
					idCollunForNameKwartz = i;
				}
			}
			// check file conform
			if (idCollunGroupKwartz == -1 || idCollunForNameKwartz == -1 || idCollunForNameKwartz == -1 || idCollunLoginKwartz == -1) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import kwartz n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			// file conform create list off all division present in file
			// Create HasSet (never make doublon)
			HashSet<String> listOfgroup = new HashSet<>();
			for (int i = 1; i < csvReaderKwartzList.size(); i++) {
				String nextLine[] = csvReaderKwartzList.get(i);
				listOfgroup.add(nextLine[idCollunGroupKwartz]);
			}
			// sort group and put to Jlist
			String[] groups = new String[listOfgroup.size()];
			groups = listOfgroup.toArray(groups);
			Arrays.sort(groups);
			listSelectGoodClasse.setListData(groups);
			// set flag
			fileImportKwartzChoose = true;
			// enable jlist show and lbl
			scrollPaneSelectGoodClasse.setVisible(true);
			lblSelectGoodClasse.setVisible(true);
			textFieldFileImportKwartz.setText(fileImportKwartz);
			textFieldFileImportKwartz.setForeground(Color.GREEN);
			// check allfileselected
			checkIsAllFileSelected();
		} catch (IOException e) {
			System.out.println("IOException");
			fileImportKwartz = null;
		}
	}

	private void chooseImportEdulineAction() {
		// set default
		hashImportByEdulineLogin = new HashMap<>();
		csvReaderEdulineList = new ArrayList<>();
		fileImportEdulineChoose = false;
		textFieldFileImportEleve.setText("Non choisi");
		textFieldFileImportEleve.setForeground(Color.RED);
		// update checkallfile to default
		checkIsAllFileSelected();
		// creater filters and get fileImport
		String[] filters = { "txt", "csv" };
		fileImportEduline = fileIO.getOpenFilePath("Fichier Eduline", filters);
		if (fileImportEduline == null) {
			JOptionPane.showMessageDialog(null, "Merci de choisir un fichier", "Invalide", JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			CSVReader csvReaderEduline = new CSVReader(new FileReader(fileImportEduline), ';');
			csvReaderEdulineList = csvReaderEduline.readAll();
			csvReaderEduline.close();
			// first check if header exist!
			if (csvReaderEdulineList.size() == 0) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import d'Eduline n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			String[] firstLine = csvReaderEdulineList.get(0);
			// iterate header
			// but first reset to -1 id !!
			idCollunNameEduline = -1;
			idCollunForNameEduline = -1;
			idCollunGroupEduline = -1;
			idCollunBirdayEduline = -1;
			for (int i = 0; i < firstLine.length; i++) {
				if (firstLine[i].equalsIgnoreCase("DIV.") || firstLine[i].equalsIgnoreCase("Division")) {
					idCollunGroupEduline = i;
				}
				if (firstLine[i].equalsIgnoreCase("NOM")) {
					idCollunNameEduline = i;
				}
				if (firstLine[i].equalsIgnoreCase("PRENOM") || firstLine[i].equalsIgnoreCase("Pr�nom 1")) {
					idCollunForNameEduline = i;
				}
				if (firstLine[i].equalsIgnoreCase("NE(E) LE") || firstLine[i].equalsIgnoreCase("Date de naissance")) {
					idCollunBirdayEduline = i;
				}
			}
			// check file conform
			if (idCollunBirdayEduline == -1 || idCollunForNameEduline == -1 || idCollunGroupEduline == -1 || idCollunNameEduline == -1) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import d'Eduline n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			// file conform set flag
			fileImportEdulineChoose = true;
			// update value
			textFieldFileImportEleve.setText(fileImportEduline);
			textFieldFileImportEleve.setForeground(Color.GREEN);
			// update check all file
			checkIsAllFileSelected();
		} catch (IOException e) {
			fileImportEduline = null;
			e.printStackTrace();
		}
	}

	private void choosePasswordAction() {
		// set default
		hashImportByPassordLogin = new HashMap<>();
		csvReaderPasswordList = new ArrayList<>();
		fileImportPasswordChoose = false;
		textFieldFileImportPassword.setText("Non choisi");
		textFieldFileImportPassword.setForeground(Color.RED);
		// update checkallfile to default
		checkIsAllFileSelected();
		// creater filters and get fileImport
		String[] filters = { "txt", "csv" };
		fileImportPassword = fileIO.getOpenFilePath("Fichier de mot de passe", filters);
		if (fileImportPassword == null) {
			JOptionPane.showMessageDialog(null, "Merci de choisir un fichier", "Invalide", JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			CSVReader csvReaderPassword = new CSVReader(new FileReader(fileImportPassword), ';');
			csvReaderPasswordList = csvReaderPassword.readAll();
			csvReaderPassword.close();
			// first check if header exist!
			if (csvReaderPasswordList.size() == 0) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import de mot de passe n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			String[] firstLine = csvReaderPasswordList.get(0);
			// iterate header
			// first reset to -1 id !!
			idCollunLoginPassword = -1;
			idCollunPasswordPassword = -1;
			for (int i = 0; i < firstLine.length; i++) {
				String value = firstLine[i].replaceAll("\"", "");
				if (value.equalsIgnoreCase("login")) {
					idCollunLoginPassword = i;
				}
				if (value.equalsIgnoreCase("password")) {
					idCollunPasswordPassword = i;
				}
			}
			// check conform file
			if (idCollunLoginPassword == -1 || idCollunPasswordPassword == -1) {
				JOptionPane.showMessageDialog(null, "Le fichier d'import de mot de passe n'est pas conforme", "Non conforme", JOptionPane.WARNING_MESSAGE);
				return;
			}
			// fileConform
			// create hashmap with login as key and password as value
			for (int i = 1; i < csvReaderPasswordList.size(); i++) {
				String[] nextLine = csvReaderPasswordList.get(i);
				// remove double quote
				String login = nextLine[idCollunLoginPassword].replaceAll("\"", "");
				String password = nextLine[idCollunPasswordPassword].replaceAll("\"", "");
				hashImportByPassordLogin.put(login, password);
			}
			// set flag
			fileImportPasswordChoose = true;
			// set value
			textFieldFileImportPassword.setText(fileImportPassword);
			textFieldFileImportPassword.setForeground(Color.GREEN);
			// update check all file
			checkIsAllFileSelected();

		} catch (IOException e) {
			fileImportPassword = null;
			e.printStackTrace();
		}

	}

	private void demarage() {
		log = "Rapport du " + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + "\r\n";
		// check if the selection list ave be make.
		if (listSelectGoodClasse.getSelectedValuesList().size() == 0) {
			JOptionPane.showMessageDialog(null, "Merci de s�lectionner vos classes dans la liste.", "Vide", JOptionPane.WARNING_MESSAGE);
			return;
		}
		//reset publiposting list
		listExportPubliposting = new ArrayList<>();
		btnStartConversion.setName("En cours...");
		btnStartConversion.setEnabled(false);
		createHashBySelection();
		createHashWithEduline();
		updateAllKwartzUser();
		createCsvFileModifyUsers();
		createCsvFileNewUsers();
		createCsvPublipostingFile();
		createLogFile();
		btnStartConversion.setName("Conversion");
		btnStartConversion.setEnabled(true);
		JOptionPane.showMessageDialog(null, "Op�ration terminer, consultez le fichier de log pour savoir comment utiliser vos fichier.", "Terminer", JOptionPane.INFORMATION_MESSAGE);
		// open log file
		try {
			if (pathForExportFile != null) {
				File file = new File(pathForExportFile);
				if (System.getProperty("os.name").toLowerCase().contains("windows")) {
					String cmd;
					cmd = "rundll32 url.dll,FileProtocolHandler " + file.getCanonicalPath();
					Runtime.getRuntime().exec(cmd);
				} else {
					Desktop.getDesktop().edit(file);
				}
			}
		} catch (IOException e) {
		}
	}

	private void createHashBySelection() {
		int accountImported = 0;
		List<String> selectedElement = listSelectGoodClasse.getSelectedValuesList();
		// set as new
		hashImportByKwartzLogin = new HashMap<>();
		String[] nextEntry;
		// iterate csvReaderList, header already read
		for (int i = 1; i < csvReaderKwartzList.size(); i++) {
			nextEntry = csvReaderKwartzList.get(i);
			if (selectedElement.contains(nextEntry[idCollunGroupKwartz])) {
				accountImported++;
				Kwartz importBySelection = new Kwartz();
				String login = nextEntry[idCollunLoginKwartz];
				importBySelection.setName(nextEntry[idCollunNameKwartz]);
				importBySelection.setForname(nextEntry[idCollunForNameKwartz]);
				importBySelection.setLogin(login);
				importBySelection.setDivision(nextEntry[idCollunGroupKwartz]);
				hashImportByKwartzLogin.put(login, importBySelection);
			}
		}
		log = log + ("Nombre de compte import� via kwartz: " + String.valueOf(accountImported)) + "\r\n";
	}

	private void createHashWithEduline() {
		int accountImported = 0;
		// set as new
		hashImportByEdulineLogin = new HashMap<>();
		boolean warningDivisionEmpty = false;
		String[] nextEntry;
		// iterate csvReader, header already read

		for (int i = 1; i < csvReaderEdulineList.size(); i++) {
			nextEntry = csvReaderEdulineList.get(i);
			accountImported++;
			Eduline classEdulineImport = new Eduline();
			// format name and forname
			String name = nextEntry[idCollunNameEduline];
			classEdulineImport.setRealName(name);
			String forName = nextEntry[idCollunForNameEduline];
			classEdulineImport.setRealForname(forName);
			// put name to uper case
			name = name.toUpperCase();
			classEdulineImport.setName(name);
			//put all charactere after ' or - or spaca in upper case
			forName = titleTextConversion(forName);
			classEdulineImport.setForname(forName);
			String division = nextEntry[idCollunGroupEduline];
			//remember ! thank's for eduline to NOT remove =""*" character then export !!!! (only for division)
			classEdulineImport.setRealDivision(division.replaceAll("\"", "").replaceAll("=", ""));
			// normalise division
			// remove all = and " and space and all to lower case
			division = division.replaceAll("\"", "").replaceAll("=", "").replaceAll("\\s+", "").toLowerCase();
			classEdulineImport.setDivision(division);
			// format date birday
			String birday = nextEntry[idCollunBirdayEduline];
			classEdulineImport.setRealBirday(birday);
			birday = birday.replaceAll("/", "");
			classEdulineImport.setBirday(birday);
			//only add this if division not empty or contain only space
			if(!division.equals("")){
				hashImportByEdulineLogin.put(configType.getLogin(name, forName), classEdulineImport);
			}else{
				//remove 1 to accountImported and set warning to true
				warningDivisionEmpty = true;
				accountImported--;
			}
		}
		log = log + ("Nombre de compte import� via eduline: " + String.valueOf(accountImported)) + "\r\n";
		if(warningDivisionEmpty){
			log = log + "Un certain nombre de compte dans Eduline n'ont pas de division, ils ont �t� ignor�s \r\n";
		}
	}

	private void updateAllKwartzUser() {
		// debug
		int accountUpdated = 0;
		int accountRemoved = 0;
		String logSpecialLogin = "";
		String logBadBirday = "";
		String logEmptypassword = "";
		boolean warningDivisionOnlyNumber = false;
		// iterate all element of hash and compare with eduline
		for (Kwartz valueKwartzChecked : hashImportByKwartzLogin.values()) {
			for (Eduline valueEdulineChecked : hashImportByEdulineLogin.values()) {
				// compare normalized name and forname, created eduline login
				String normalisedEdulineName = normalizeString(valueEdulineChecked.getName());
				String normalisedEdulineForName = normalizeString(valueEdulineChecked.getForname());
				String normalisedKwartzName = normalizeString(valueKwartzChecked.getName());
				String normalisedKwartzForName = normalizeString(valueKwartzChecked.getForname());
				String tempLogin = configType.getLogin(normalisedEdulineName, normalisedEdulineForName);
				if ((normalisedKwartzName.equalsIgnoreCase(normalisedEdulineName) && normalisedKwartzForName.equalsIgnoreCase(normalisedEdulineForName)) || tempLogin.equalsIgnoreCase(valueKwartzChecked.getLogin())) {
					accountUpdated++;
					// if no new password selected update with old password if
					// selected
					if (!chckbxResetAllPassword.isSelected() && chckbxEnableExternalPassword.isSelected()) {
						// if hash key value exist put this to password
						if (hashImportByPassordLogin.containsKey(valueKwartzChecked.getLogin())) {
							valueKwartzChecked.setPassword(hashImportByPassordLogin.get(valueKwartzChecked.getLogin()));
						} else {
							// create new password for this user
							if (valueEdulineChecked.getBirday().length() == 8 || configType.getTypePasswordChosed().equals("Al�atoire")) {
								valueKwartzChecked.setPassword(configType.getPassword(valueEdulineChecked.getBirday()));
							} else {
								// birday not correct force a al�atory password
								String password = randomPassword.getRandomPassword();
								valueKwartzChecked.setPassword(password);
							}
							// and log
							logEmptypassword = logEmptypassword + "Nom: " + valueKwartzChecked.getName() + ",Pr�nom: " + valueKwartzChecked.getForname() + " a comme mot de passe: "
									+ valueKwartzChecked.getPassword() + "\r\n";
						}

					}
					// value find check if new password and new login needed
					if (chckbxResetAllPassword.isSelected()) {
						// check if birday is correct (only if not random
						// password chose)
						if (valueEdulineChecked.getBirday().length() == 8 || configType.getTypePasswordChosed().equals("Al�atoire")) {
							valueKwartzChecked.setPassword(configType.getPassword(valueEdulineChecked.getBirday()));
						} else {
							// birday not correct force a al�atory password
							String newPassword = randomPassword.getRandomPassword();
							// add to log only if not aleatory config
							if (!configType.getTypePasswordChosed().equals("Al�atoire")) {
								logBadBirday = logBadBirday + "Nom: " + valueKwartzChecked.getName() + ",Pr�nom: " + valueKwartzChecked.getForname() + " a comme mot de passe: " + newPassword + "\r\n";
							}
							valueKwartzChecked.setPassword(newPassword);
						}
					}
					//reset login if option activated
					if (chckbxResetAllLogin.isSelected()) {
						String newLogin = configType.getLogin(valueKwartzChecked.getName(), valueKwartzChecked.getForname());
						// windows 7 not suport more 20 character !!
						//use normalised name and forname to create new login !!!
						if (newLogin.length() > 20) {
							// check first if 2 numbers add at the end of the login
							if (!configType.isAddTwoAleatoryNumber()) {
								// two caracter NOT need add (9*2 +1 <20)
								// get 9 first character of name and 9 first
								// character of forname (only if name or forname
								// ave more 9 character)
								newLogin = configType.getLogin((normalisedKwartzName.length() <= 9) ? normalisedKwartzName : normalisedKwartzName.substring(0, 9),
										(normalisedKwartzForName.length() <= 9) ? normalisedKwartzForName : normalisedKwartzForName.substring(0, 9));
							} else {
								// two caracter need add (8*2 +1 +2 <20)
								// get 8 first character of name and 8 first
								// character of forname (only if name or forname
								// ave more 8 character)
								newLogin = configType.getLogin((normalisedKwartzName.length() <= 8) ? normalisedKwartzName : normalisedKwartzName.substring(0, 8),
										(normalisedKwartzForName.length() <= 8) ? normalisedKwartzForName : normalisedKwartzForName.substring(0, 8));
							}
							logSpecialLogin = logSpecialLogin + "Nom: " + valueKwartzChecked.getName() + ",Pr�nom: " + valueKwartzChecked.getForname() + " a comme login: " + newLogin + "\r\n";
						}
						valueKwartzChecked.setNewLogin(newLogin);
					}
					//reset name and forname if option selected
					if(chckbxResetKwarzNameAndForname.isSelected()){
						valueKwartzChecked.setName(valueEdulineChecked.getRealName());
						valueKwartzChecked.setForname(valueEdulineChecked.getRealForname());
					}
					// set new division
					String division = valueEdulineChecked.getDivision();
					if(division.matches("[0-9]+")){
						division = configType.getDefaultPrefix()+division;
						warningDivisionOnlyNumber =true;
					}
					valueKwartzChecked.setDivision(division);
					// set need not removing for kwartz and not new for eduline
					valueKwartzChecked.setNeedDelete(false);
					valueEdulineChecked.setIsNew(false);
					//now add this for publiposting use real name, real forname, real division and login or new login and password
					Publiposting exportPubliposting = new Publiposting();
					exportPubliposting.setName(valueEdulineChecked.getRealName());
					exportPubliposting.setForName(valueEdulineChecked.getRealForname());
					//check correct birday
					if (valueEdulineChecked.getBirday().length() == 8){
						exportPubliposting.setBirday(valueEdulineChecked.getRealBirday());
					}else{
						exportPubliposting.setBirday("Invalide");
					}
					exportPubliposting.setDivision(valueEdulineChecked.getRealDivision());
					//check if is new login or login used
					if (chckbxResetAllLogin.isSelected()) {
						exportPubliposting.setLogin(valueKwartzChecked.getNewLogin());
					}else{
						exportPubliposting.setLogin(valueKwartzChecked.getLogin());
					}
					exportPubliposting.setPassword(valueKwartzChecked.getPassword());
					listExportPubliposting.add(exportPubliposting);
					break;
				}
			}
		}
		// set now all login not update to division 'compte_a_verifier'
		for (Kwartz valueKwartzChecked : hashImportByKwartzLogin.values()) {
			if (valueKwartzChecked.needDelete()) {
				accountRemoved++;
				valueKwartzChecked.setDivision("compte_a_verif");
			}
		}
		log = log + "Nombre de compte modifi� dans le fichier import� de kwartz: " + String.valueOf(accountUpdated) + "\r\n";
		if(warningDivisionOnlyNumber){
			log = log +"Un certain nombre de division ne contenait que des chiffres, le pr�fixe: '"+configType.getDefaultPrefix()+"' � �t� rajout�\r\n";
		}
		if (logSpecialLogin != "") {
			log = log + "Windows 7 ne supporte pas plus de 20 caracteres pour le login.\r\nLes comptes suivant ont un login sp�cifique:\r\n";
			log = log + logSpecialLogin;
		}
		if (logBadBirday != "") {
			log = log + "Certaine date de naissance ne sont pas correcte, des mots de passe al�atoire ont �t� substitu�:\r\n";
			log = log + logBadBirday;
		}
		if (logEmptypassword != "") {
			log = log + "Vous avez s�l�ctionnez un fichier avec des mots de passe mais certain compte n'ont pas �t� trouv�.\r\nLes comptes suivant ont un mot de passe sp�cifique:\r\n";
			log = log + logEmptypassword;
		}
		log = log + "Nombre de compte d�plac� dans 'compte_a_verif' (Kwartz n'appr�cie pas compte_a_verifier): " + String.valueOf(accountRemoved) + "\r\n";
	}

	private void createCsvFileModifyUsers() {
		String[] filters = { "txt" };
		String pathForExportFile = fileIO.getFileSaveChooser("Export mise � jours des �l�ves", filters,
				"Liste_des_eleves_a_modifier_le_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".txt");
		while (pathForExportFile == null) {
			JOptionPane.showMessageDialog(null, "Vous devez imp�rativement sauvegarder ce fichier.", "Obligation", JOptionPane.WARNING_MESSAGE);
			pathForExportFile = fileIO.getFileSaveChooser("Export mise � jours des �l�ves", filters,
					"Liste_des_eleves_a_modifier_le_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".txt");
		}
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(pathForExportFile));
			// (char) 0 --> to indicate at csvwriter not use quote character !!
			CSVWriter csvWriter = new CSVWriter(out, ';', (char) 0, "\r\n");
			// prepare header
			String[] header = { "#Nom", "Pr�nom", "groupe d'affectation", "login actuel", "nouveau login", "mot de passe", "droits", "groupes invit�s", "groupes responsable", "serveur mail externe",
					" login mail externe", " mot de passe mail externe", " adresse email", " identifiant externe", " profil windows", " profil d'acc�s � internet", " compte d�sactiv�" };
			csvWriter.writeNext(header);
			// iterate list
			for (Kwartz valueKwartzChecked : hashImportByKwartzLogin.values()) {
				String name = valueKwartzChecked.getName();
				String forName = valueKwartzChecked.getForname();
				String division = valueKwartzChecked.getDivision();
				String login = valueKwartzChecked.getLogin();
				String newLogin = valueKwartzChecked.getNewLogin();
				String password = valueKwartzChecked.getPassword();
				// never set new login if this is the old ...
				//ERREUR !!!!!!
//				if (newLogin.equals(login)) {
//					newLogin = "";
//				}
				String[] newLine = { name, forName, division, login, newLogin, password, "", "", "", "", "", "", "", "", "", "", "" };
				csvWriter.writeNext(newLine);
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void createCsvFileNewUsers() {
		String logBadBirday = "";
		String logSpecialLogin = "";
		int accountAdd = 0;
		boolean warningDivisionOnlyNumber = false;
		String[] filters = { "csv,txt" };
		String pathForExportFile = fileIO.getFileSaveChooser("Export nouveaux �l�ves", filters, "Liste_des_eleves_a_ajouter_le_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime())
				+ ".txt");
		while (pathForExportFile == null) {
			JOptionPane.showMessageDialog(null, "Vous devez imp�rativement sauvegarder ce fichier.", "Obligation", JOptionPane.WARNING_MESSAGE);
			pathForExportFile = fileIO.getFileSaveChooser("Export nouveaux �l�ves", filters, "Liste_des_eleves_a_ajouter_le_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime())+ ".txt");
		}
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(pathForExportFile));
			// (char) 0 --> to indicate at csvwriter not use quote character !!
			CSVWriter csvWriter = new CSVWriter(out, ';', (char) 0, "\r\n");
			// prepare header
			String[] header = { "#Nom", "Pr�nom", "groupe d'affectation", "login actuel", "nouveau login", "mot de passe", "droits", "groupes invit�s", "groupes responsable", "serveur mail externe",
					" login mail externe", " mot de passe mail externe", " adresse email", " identifiant externe", " profil windows", " profil d'acc�s � internet", " compte d�sactiv�" };
			csvWriter.writeNext(header);
			for (Eduline valueEdulineChecked : hashImportByEdulineLogin.values()) {
				// add only element not set to not new
				if (valueEdulineChecked.isNew()) {
					accountAdd++;
					String name = valueEdulineChecked.getName();
					String forName = valueEdulineChecked.getForname();
					String division = valueEdulineChecked.getDivision();
					//add suffix if division contain only number
					if(division.matches("[0-9]+")){
						division = configType.getDefaultPrefix()+division;
						warningDivisionOnlyNumber =true;
					}
					String password = "";
					// check if birday correctly set
					if (configType.getTypePasswordChosed().equals("Al�atoire") || valueEdulineChecked.getBirday().length() == 8) {
						password = configType.getPassword(valueEdulineChecked.getBirday());
					} else {
						// birday not correctly set force all case random
						// password
						password = randomPassword.getRandomPassword();
						// add to log only if not aleatory config
						if (!configType.getTypePasswordChosed().equals("Al�atoire")) {
							logBadBirday = logBadBirday + "Nom: " + name + ",Pr�nom: " + forName + " a comme mot de passe: " + password + "\r\n";
						}
					}
					String login = configType.getLogin(name, forName);
					// check if login ave more 20 char windows 7 limit
					if (login.length() > 20) {
						// check first if 2 numbers add at the end of the
						// login
						if (!configType.isAddTwoAleatoryNumber()) {
							// no two caracter need add (9*2 +1 <20)
							// get 9 first character of name and 9 first
							// character of forname (only if name or forname
							// ave more 9 character)
							login = configType.getLogin((name.length() <= 9) ? name : name.substring(0, 9), (forName.length() <= 9) ? forName : forName.substring(0, 9));
						} else {
							// two caracter need add (8*2 +1 +2 <20)
							// get 8 first character of name and 8 first
							// character of forname (only if name or forname
							// ave more 8 character)
							login = configType.getLogin((name.length() <= 8) ? name : name.substring(0, 8), (forName.length() <= 8) ? forName : forName.substring(0, 8));
						}
						// add to log
						logSpecialLogin = logSpecialLogin + "Nom: " + name + ",Pr�nom: " + forName + " a comme login: " + login + "\r\n";

					}
					String[] newLine = { name, forName, division, login, login, password, "", "", "", "", "", "", "", "", "", "", "" };
					csvWriter.writeNext(newLine);
					//now add this for publiposting use real name, real forname, real division and login and password
					Publiposting exportPubliposting = new Publiposting();
					exportPubliposting.setName(valueEdulineChecked.getRealName());
					exportPubliposting.setForName(valueEdulineChecked.getRealForname());
					//check correct birday
					if (valueEdulineChecked.getBirday().length() == 8){
						exportPubliposting.setBirday(valueEdulineChecked.getRealBirday());
					}else{
						exportPubliposting.setBirday("Invalide");
					}
					exportPubliposting.setDivision(valueEdulineChecked.getRealDivision());
					//check if is new login or login used
					exportPubliposting.setLogin(login);
					exportPubliposting.setPassword(password);
					listExportPubliposting.add(exportPubliposting);
				}
			}
			csvWriter.flush();
			csvWriter.close();
			// Create log
			log = log + "Nombre de compte ajout� � partir du fichier Eduline: " + String.valueOf(accountAdd) + "\r\n";
			if(warningDivisionOnlyNumber){
				log = log +"Un certain nombre de division ne contenait que des chiffres, le pr�fixe: '"+configType.getDefaultPrefix()+"' � �t� rajout�\r\n";
			}
			if (logSpecialLogin != "") {
				log = log + "Windows 7 ne supporte pas plus de 20 caracteres pour le login.\r\nLes comptes suivant ont un login sp�cifique:\r\n";
				log = log + "ATTENTION la cr�ation du login d�pendant du logiciel utilis� certain doublon peuvent apparaitre.\r\n";
				log = log + logSpecialLogin;
			}
			if (logBadBirday != "") {
				log = log + "Certaine date de naissance ne sont pas correcte, des mots de passe al�atoire ont �t� substitu�:\r\n";
				log = log + logBadBirday;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createCsvPublipostingFile(){
		String[] filters = { "txt", "csv" };
		String pathForPublipostingFile = fileIO.getFileSaveChooser("Publipostage", filters, "Export_pour_publipostage_du" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".csv");
		while (pathForPublipostingFile == null) {
			JOptionPane.showMessageDialog(null, "Vous devez imp�rativement sauvegarder ce fichier.", "Obligation", JOptionPane.WARNING_MESSAGE);
			pathForPublipostingFile = fileIO.getFileSaveChooser("Publipostage", filters, "Export_pour_publipostage_du" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".csv");
		}

		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(pathForPublipostingFile));
			// (char) 0 --> to indicate at csvwriter not use quote character !!
			CSVWriter csvWriter = new CSVWriter(out, ';', (char) 0, "\r\n");
			// prepare header
			String[] header = { "Nom", "Pr�nom", "Division", "Date de naissance", "Identifiant", "Mot de passe"};
			csvWriter.writeNext(header);
			//iterate list
			for(Publiposting pupliposting:listExportPubliposting){
				String[] nextLine = { pupliposting.getName(), pupliposting.getForName(), pupliposting.getDivision(), pupliposting.getBirday(), pupliposting.getLogin(), pupliposting.getPassword()};
				csvWriter.writeNext(nextLine);
			}
			csvWriter.flush();
			csvWriter.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
	}

	
	private void createLogFile() {
		// add end of log first
		log = log + endLog;
		String[] filters = { "txt", "csv" };
		pathForExportFile = fileIO.getFileSaveChooser("R�sultat op�ration", filters, "Log_de_l_operation_du_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".txt");
		while (pathForExportFile == null) {
			JOptionPane.showMessageDialog(null, "Vous devez imp�rativement sauvegarder ce fichier.", "Obligation", JOptionPane.WARNING_MESSAGE);
			pathForExportFile = fileIO.getFileSaveChooser("R�sultat op�ration", filters, "Log_de_l_operation_du_" + new SimpleDateFormat("dd-MM-yy").format(new Date().getTime()) + ".txt");
		}
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(pathForExportFile));
			writer.write(log);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String normalizeString(String value) {
		// put all to lower case and delete special character
		return Normalizer.normalize(value.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("[\\W_]", "");
	}
	
    public static String titleTextConversion(String text) {
    	text = Normalizer.normalize(text.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        String[] words = text.split("(?<=\\W)|(?=\\W)");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            if (word.length() > 0)
                sb.append(word.substring(0, 1).toUpperCase()).append(word.substring(1).toLowerCase());
        }
        return sb.toString();
    }
	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path,
	                                           String description) {
	    java.net.URL imgURL = getClass().getResource(path);
	    if (imgURL != null) {
	        return new ImageIcon(imgURL, description);
	    } else {
	        System.err.println("Couldn't find file: " + path);
	        return null;
	    }
	}
}
