package com.zimko.EdulineToKwartz.FilesManagement;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileIO {
	private String currentDirectory;
	public FileIO(){
		this.currentDirectory = System.getProperty("user.dir");
	}
	public String getOpenFilePath(String nameOfInputDialog, String[] filters) {

		JFileChooser fileChooser = new JFileChooser(new File(currentDirectory));
		SetFilter Fc = new SetFilter(filters);
		fileChooser.addChoosableFileFilter(Fc);
		fileChooser.setFileFilter(Fc);
		fileChooser.setAcceptAllFileFilterUsed(false);
		// pop up an "Open File" file chooser dialog
		int retrival = fileChooser.showDialog(null, nameOfInputDialog);
		if (retrival == JFileChooser.CANCEL_OPTION) {
			return null;
		}
		if (!fileChooser.getSelectedFile().exists()) {
			return null;
		}
		// set currentDir
		currentDirectory = fileChooser.getSelectedFile().getAbsolutePath().substring(0, fileChooser.getSelectedFile().getAbsolutePath().lastIndexOf(File.separator));
		return fileChooser.getSelectedFile().getAbsolutePath();
	}

	public String getFileSaveChooser(String nameOfInputDialog, String[] filters, String defaultName) {
		JFileChooser fileChooser = new JFileChooser(new File(currentDirectory));
		SetFilter Fc = new SetFilter(filters);
		fileChooser.addChoosableFileFilter(Fc);
		fileChooser.setFileFilter(Fc);
		fileChooser.setAcceptAllFileFilterUsed(false);
		// pop up an "Open File" file chooser dialog
		fileChooser.setSelectedFile(new File(defaultName));
		int retrival = fileChooser.showDialog(null, nameOfInputDialog);
		if (retrival == JFileChooser.APPROVE_OPTION) {
			if (fileChooser.getSelectedFile().exists()) {
				int result = JOptionPane.showConfirmDialog(null, "Etes vous sur de supprimer le fichier ?", "Attention", JOptionPane.OK_CANCEL_OPTION);
				if (result == JOptionPane.OK_OPTION) {
					currentDirectory = fileChooser.getSelectedFile().getAbsolutePath().substring(0, fileChooser.getSelectedFile().getAbsolutePath().lastIndexOf(File.separator));
					return fileChooser.getSelectedFile().getAbsolutePath();
				} else {
					return null;
				}
			} else {
				currentDirectory = fileChooser.getSelectedFile().getAbsolutePath().substring(0, fileChooser.getSelectedFile().getAbsolutePath().lastIndexOf(File.separator));
				return fileChooser.getSelectedFile().getAbsolutePath();
			}
		} else {
			return null;
		}

	}
}
