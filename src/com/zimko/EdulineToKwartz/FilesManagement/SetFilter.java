package com.zimko.EdulineToKwartz.FilesManagement;

import java.io.File;

import javax.swing.filechooser.*;

public class SetFilter extends FileFilter {
	private String[] extensionsAvailable;
	public SetFilter(String[] extensionsAvailable){
		this.extensionsAvailable = extensionsAvailable;
	}
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String extension = SetFilter.getExtension(f);
        if (extension != null) {
        	for(int i=0;i<extensionsAvailable.length;i++){
        		String extensionAvailable = extensionsAvailable[i];
        		if(extension.equalsIgnoreCase(extensionAvailable)){
        			return true;
        		}
        	}
        }
        return false;
    }

    //The description of this filter
    public String getDescription() {
    	String returnValue = extensionsAvailable[0];
    	for(int i=1;i<extensionsAvailable.length;i++){
    		returnValue = "*."+returnValue+",";
    		//TODO creer la description !!!!
    		String extensionAvailable = extensionsAvailable[i];
    		returnValue = returnValue + "*."+extensionAvailable;
    	}
        return returnValue;
    }
    
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
}
