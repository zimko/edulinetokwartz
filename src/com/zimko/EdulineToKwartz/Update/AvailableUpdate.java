package com.zimko.EdulineToKwartz.Update;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.zimko.EdulineToKwartz.EdulineToKwartz;

public class AvailableUpdate implements Runnable  {
	private EdulineToKwartz main;
	private String urlChecked = "https://bitbucket.org/zimko/edulinetokwartz/downloads/version.xml";
	double newVersion=0;
	String link="";
	public AvailableUpdate(EdulineToKwartz instance) {
		this.main = instance;
		 System.setProperty("java.net.useSystemProxies","true");
	}

	@Override
	public void run() {
		StringBuilder responseBuilder = getUrlXml();
		if (responseBuilder != null) {
			SAXBuilder builder = new SAXBuilder();
			try {
				Document document = builder.build(new ByteArrayInputStream(responseBuilder.toString().getBytes()));
				Element rootNode = document.getRootElement();
				List<Element> list = rootNode.getChildren("version");
				for (int i = 0; i < list.size(); i++) {
					Element node = (Element) list.get(i);
					newVersion= Double.parseDouble(node.getChildText("number"));
					link =node.getChildText("link");
				}
				if(newVersion!=0 && !link.equals("")){
					if(newVersion>main.version){
						//new version available
						main.lblUpdateAvailable.setText("Nouvelle version disponible: "+newVersion);
						main.lblUpdateAvailable.setForeground(Color.ORANGE);
						main.lblLinkUpdate.setText(link);
						main.lblLinkUpdate.setVisible(true);
					}
				}
			} catch (JDOMException | IOException jdomex) {
				System.out.println(jdomex.getMessage());
			}
		}else{
			//server connection erreur
			main.lblUpdateAvailable.setText("Serveur de mise � jours indisponible");
			main.lblUpdateAvailable.setForeground(Color.CYAN);
		}
	}

	public StringBuilder getUrlXml() {
		StringBuilder responseBuilder = new StringBuilder();
		try {
			// Create a URLConnection object for a URL
			URL url = new URL(urlChecked);

			//BufferedReader rd = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			BufferedReader rd = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;

			while ((line = rd.readLine()) != null) {
				responseBuilder.append(line + '\n');
			}
			return responseBuilder;
		} catch (Exception e) {
			return null;
		}
	}

}
