package com.zimko.EdulineToKwartz;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;

import java.awt.Color;

public class VersionGui extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7472132780217772860L;

	public VersionGui(String versionning) {
		setModal(true);
		setResizable(false);
		setType(Type.POPUP);
		setIconImage(Toolkit.getDefaultToolkit().getImage(VersionGui.class.getResource("/javax/swing/plaf/metal/icons/Inform.gif")));
		setTitle("Info version");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 279, 365);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBackground(Color.GRAY);
		getContentPane().add(desktopPane, BorderLayout.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 268, 331);
		desktopPane.add(scrollPane);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(false);
		editorPane.setContentType("text/html");
		editorPane.setText(versionning);
		editorPane.setCaretPosition(0);
		scrollPane.setViewportView(editorPane);
		this.setVisible(true);
	}
}
